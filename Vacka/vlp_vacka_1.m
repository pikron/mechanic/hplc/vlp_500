AA=12;    % uhel na rozbehy a dobehy
AB=15;    % zacatek nabehu
AE=165;   % konec nabehu
DB=195;   % zacatek sestupu
DE=345;   % konec sestupu
PL=7+8;   % nejnizsi poloha
PH=20+8;  % nejvyssi poloha

D=0.1;

% Vzestup
% 2 * (1/2) * Aacc * AA ^ 2 + (AE - AB - 2 * AA) * Aslp = PH - PL
% Aacc * AA = Aslp
%
% Aacc * AA ^ 2 + (AE - AB -2 * AA) * Aacc * AA = PH - PL
% Aacc = (PH - PL) / (AA ^ 2 + (AE - AB - 2 * AA) * AA)


Aacc=(PH-PL)/(AA^2+(AE-AB-2*AA)*AA);
Aslp=Aacc*AA;
Dacc=(PH-PL)/(AA^2+(DE-DB-2*AA)*AA);
Dslp=Dacc*AA;


A=[0:D:360]';
P=zeros(size(A));

for i = 1:length(A)
  if (A(i)>=0) & (A(i)<=AB)
    P(i)=PL;
  elseif (A(i)>=AB) & (A(i)<=AB+AA)
    a=(A(i)-AB);
    P(i)=PL+1/2*Aacc*a^2;
  elseif (A(i)>=AB+AA) & (A(i)<=AE-AA)
    a=(A(i)-AB-AA);
    P(i)=PL+1/2*Aacc*AA^2+Aslp*a;
  elseif (A(i)>=AE-AA) & (A(i)<=AE)
    a=(A(i)-AE);
    P(i)=PH-1/2*Aacc*a^2;


  elseif (A(i)>=AE) & (A(i)<=DB)
    P(i)=PH;
  elseif (A(i)>=DB) & (A(i)<=DB+AA)
    a=(A(i)-DB);
    P(i)=PH-1/2*Aacc*a^2;
  elseif (A(i)>=DB+AA) & (A(i)<=DE-AA)
    a=(A(i)-DB-AA);
    P(i)=PH-1/2*Aacc*AA^2-Aslp*a;
  elseif (A(i)>=DE-AA) & (A(i)<=DE)
    a=(A(i)-DE);
    P(i)=PL+1/2*Aacc*a^2;

  elseif (A(i)>=DE)
    P(i)=PL;
  end
end

figure(1)
dP=[0;diff(P)];
ddP=[0;0;diff(diff(P))];
kdP=(max(P)-min(P))/(max(dP)-min(dP));
kddP=(max(P)-min(P))/(max(ddP)-min(ddP));
plot(A,P,'r-',A,dP*kdP,'g--',A,ddP*kddP,'b:');

PA=[A,P];

save('vlp_vacka_1.dat','-ascii','PA');
